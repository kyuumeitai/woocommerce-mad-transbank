jQuery(function($){

	$('.reversepayment').click(function(e){
		var nonce = $(this).data('nonce');
		var buyorder = $(this).data('buyorder');
		var paymentid = $(this).data('paymentid');
		console.log('submitted', nonce, buyorder);
		$.post( ajaxurl, {
			action: 'mad_tbk_reverse',
			nonce,
			buyorder,
			paymentid
		}, function(data){
			// {"reverseCode":3159807660031012922,"reversed":true}
			if(data.reversed){
				window.location.reload(true);
			}
		}, 'json');
		e.preventDefault();
	});
});