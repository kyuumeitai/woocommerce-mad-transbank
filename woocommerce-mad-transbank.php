<?php
/**
 * Plugin Name: WooCommerce MAD Transbank Gateway
 * Plugin URI: https://mad.cl
 * Description: Provides a Gateway to Transbank.cl chilean SUPER payment system
 * Version: 0.1 beta
 * Author: Álex Acuña Viera | Alejandro Sánchez
 * Author URI: https://alex.acunaviera.com
 */

/**
 * @class       WC_Mad_Transbank
 * @extends     WC_Payment_Gateway
 * @version     1.0.0
 * @package     WooCommerce/Classes/Payment
 * @author      MAD
 */

defined( 'ABSPATH' ) or exit;

require_once(ABSPATH.'wp-includes/pluggable.php');

// Ver si woocommerce está instalado y activo
if ( ! in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) return;

add_action('plugins_loaded', 'wc_mad_transbank_init', 0);

function wc_mad_transbank_add_to_gateways( $gateways ) {
    $gateways[] = 'WC_Mad_Transbank';
    return $gateways;
}

add_filter( 'woocommerce_payment_gateways', 'wc_mad_transbank_add_to_gateways' );

//removes the other payment methods if subscription is in cart

function unset_no_suscription($available_gateways){
		global $woocommerce;
    if( ! $woocommerce || !isset($woocommerce->cart) ) 
        return $available_gateways;

    $suscription = false;
    $no_susc = array();
    foreach(WC()->cart->get_cart() as $id=>$item){
        if($suscription == false && has_term('subscription', 'product_cat', $item['product_id'])){
            $suscription = true;
        }else{
            $no_susc[] = $id;
        }
    
    }
    if($suscription){
        foreach($available_gateways as $ix=>$gateways){
            if($ix !== 'madtransbank'){
                unset($available_gateways[$ix]);
            }
        }
        foreach($no_susc as $id=>$key){
            WC()->cart->remove_cart_item($key);
        }
    }
    return $available_gateways;
}

//add_filter( 'woocommerce_available_payment_gateways', 'unset_no_suscription', 10, 1 ); 

function wc_mad_transbank_plugin_links( $links ) {
    $plugin_links = array(
        '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout&section=mad_transbank_gateway' ) . '">' . 'Configurar' . '</a>'
    );
    return array_merge( $plugin_links, $links );
}
add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'wc_mad_transbank_plugin_links' );

function mad_register_payment_posttype() {
    register_post_type( 'mad_payments',
        array(
            'labels' => array(
                'name' => 'Pagos',
                'singular_name' => 'Pago'
                ),
        'public' => true,
        'has_archive' => false
        )
    );
}
add_action( 'init', 'mad_register_payment_posttype' );

function mad_rewrite_flush() {
    mad_register_payment_posttype();
    flush_rewrite_rules();
}

register_activation_hook( __FILE__, 'mad_rewrite_flush' );


function mad_payments_scripts() {
    wp_register_script('madpaymentscript', plugin_dir_url( __FILE__ ) . 'main.js' , array('jquery'), '0.1', true);
    wp_enqueue_script('madpaymentscript');
}

add_action('admin_enqueue_scripts', 'mad_payments_scripts');

function mad_add_payments_meta_box() {
    add_meta_box(
        'mad_payments_box', 'Pagos para esta orden',
        'mad_payments_metabox_callback',
        'shop_order',
        'normal',
        'default'
    );
}


add_action('wp_ajax_mad_tbk_reverse', 'mad_ajax_tbk_reverse');

function mad_ajax_tbk_reverse(){
	$buyOrder = $_REQUEST['buyorder'];	
	$nonce = $_REQUEST['nonce'];
	$paymentid = $_REQUEST['paymentid'];
	$errors = [];
	if(!wp_verify_nonce( $nonce, 'payment-nonce')){
		header('Content-Type: application/json');
		$errors['nonce'] = true;
		echo json_encode($errors);
		exit;
	}
	$madgateway = new WC_Mad_Transbank();
	$response = $madgateway->reverse_payment($buyOrder);
	// && $response === TRUE)
	if(!empty($response)){
		header('Content-Type: application/json');
		if(!empty($response->reversed) && $response->reversed === true){
			update_post_meta( $paymentid, 'status', 'Reversado');
			update_post_meta( $paymentid, 'reverse_id', $response->reverseCode);
		}
		echo json_encode($response);
		exit;
	}
	header('Content-Type: application/json');
	echo json_encode( array() );
	exit;
}

function mad_payments_metabox_callback( $post, $metabox ) {
	$payments = get_post_meta($post->ID, '_payment');
	$userid = get_post_meta($post->ID, '_customer_user', true);

/*
	echo '<pre>';
// 		print_r(get_user_meta($userid));
		the_field('field_599492332d6f7', 'user_'.$userid);
		// Autorizacion TBK
    the_field('field_599489b42d6f3', 'user_'.$userid);             
    // Tipo Tarjeta
    the_field('field_599489e72d6f4', 'user_'.$userid); 
    // Ultimos 4 digitos
    the_field('field_599491f52d6f5', 'user_'.$userid);     
    // Usuario TBK
		the_field('field_599492092d6f6', 'user_'.$userid); 
	echo '</pre>';
*/
	if(!empty($payments)){
		?>
		<table class="wp-list-table widefat fixed striped posts">
			<thead>
				<tr>
						<th scope="col">
							<span>Fecha del pago</span>
						</th>
						<th scope="col">
							<span>Monto</span>
						</th>
						<th scope="col">
							<span>Status</span>
						</th>
						<th scope="col"s class="column-primary">
							<span>Acciones</span>
						</th>
				</tr>
			</thead>

		<?php 
		foreach($payments as $paymentid){
			$payment = get_post($paymentid);
			$status = get_post_meta($paymentid, 'status', true);
			$nonce = wp_create_nonce('payment-nonce');
			?>
			<tr>
				<td><?php echo $payment->post_date;  ?></td>
				<td>$<?php echo get_post_meta($paymentid, 'amount', true) ?></td>
				<td><?php echo $status; ?></td>
				<td>
					<?php 
						if($status === 'Cobrado'){
							?>
							Orden de compra: <br /><strong><?php echo get_post_meta($paymentid, 'buyOrder', true);?></strong><br />
							
							<a href="#" class="button reversepayment" data-nonce="<?php echo $nonce; ?>" data-buyorder="<?php echo get_post_meta($paymentid, 'buyOrder', true); ?> " data-paymentid="<?php echo $paymentid; ?>">Reversar cobro</a>
							<?php
						}
						elseif($status === 'Reversado'){
							?>
							Orden de compra: <br /><strong><?php echo get_post_meta($paymentid, 'buyOrder', true);?></strong><br />
							Código de reversa: <br /><strong><?php echo get_post_meta($paymentid, 'reverse_id', true);?></strong>
							<?php
						}
						elseif($status === 'Rechazo'){
							?>
							Orden de compra: <br /><strong><?php echo get_post_meta($paymentid, 'buyOrder', true);?></strong>
							<?php
						}
					?>
					
				</td>
			</tr>
			<?php
		}
		?>
		</table>
		<?php 
	} else {
		echo 'Aún no se registran pagos con Tarjeta de Crédito para esta suscripción';
	}
}

add_action( 'add_meta_boxes', 'mad_add_payments_meta_box' );


function wc_mad_transbank_init() {    
    if (!class_exists("WC_Payment_Gateway")){
        return;
    }
    require_once('webpay-sdk/libwebpay/webpay.php');
    
    class WC_Mad_Transbank extends WC_Payment_Gateway {
        
        private $wp_service;
        
        public function __construct() {
            include('webpay-sdk/sample/certificates/cert-oneclick.php');
            $this->id                   = 'madtransbank';
            $this->icon                 = apply_filters('woocommerce_mad_transbank', '');
            $this->has_fields           = false;
            $this->method_title         = __('Mad Transbank', 'wc-mad-transbank');
            $this->method_description   = __('Pasarela de pago para Transbank', 'wc-mad-transbank');
            $this->notify_url           = add_query_arg('wc-api', 'WC_Gateway_' . $this->id, home_url('/'));
            $this->certificate            = $certificate;
            $this->init_form_fields();
            $this->init_settings();

            $this->title                = $this->get_option('title');
            $this->description          = $this->get_option('description');
            $this->instructions         = $this->get_option('instructions', $this->description);

            $this->config = array(
                "MODO" => $this->get_option('webpay_test_mode'),
                "PRIVATE_KEY" => $this->get_option('webpay_private_key'),
                "PUBLIC_CERT" => $this->get_option('webpay_public_cert'),
                "WEBPAY_CERT" => $this->get_option('webpay_webpay_cert'),
                "CODIGO_COMERCIO" => $this->get_option('webpay_commerce_code'),
                "URL_RETURN" => home_url('/') . '?wc-api=WC_Gateway_' . $this->id,
                "URL_FINAL" => "_URL_"
            );
            $this->initWpService($this->get_option('ambiente'));
            add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
            add_action('woocommerce_receipt_' . $this->id, array($this, 'receipt_page'));
            add_action('woocommerce_thankyou_' . $this->id, array($this, 'thankyou_page'));
            add_action('woocommerce_email_before_order_table', array($this, 'email_instructions'), 10, 3);
            add_action('woocommerce_api_wc_gateway_' . $this->id, array($this, 'check_ipn_response'));

            if (!$this->is_valid_for_use()) {
                $this->enabled = false;
            }
        }
        
        function initWpService($ambiente = 'INTEGRACION'){
            $configuration = new configuration();    
            if($ambiente == 'INTEGRACION'){
                $configuration->setEnvironment($this->certificate['environment']);
                $configuration->setCommerceCode($this->certificate['commerce_code']);
                $configuration->setPrivateKey($this->certificate['private_key']);
                $configuration->setPublicCert($this->certificate['public_cert']);
                $configuration->setWebpayCert($this->certificate['webpay_cert']);
            }else{
                $configuration->setEnvironment($ambiente);
                $configuration->setCommerceCode($this->get_option('webpay_commerce_code'));
                $configuration->setPrivateKey($this->get_option('webpay_private_key'));
                $configuration->setPublicCert($this->get_option('webpay_public_cert'));
                $configuration->setWebpayCert($this->get_option('webpay_webpay_cert'));
            }
            $this->wp_service = new Webpay($configuration);
        }
        
        public function is_valid_for_use(){
            if (!in_array(get_woocommerce_currency(), apply_filters('woocommerce_' . $this->id . '_supported_currencies', array('CLP')))) {
                return false;
            }
            return true;
        }

        public function init_form_fields() {

            $this->form_fields = apply_filters( 'wc_mad_transbank_init', array(
                'enabled' => array(
                    'title'   => __( 'Habilitado/Deshabilitado', 'wc-mad-transbank' ),
                    'type'    => 'checkbox',
                    'label'   => __( 'Habilita Mad Transbank', 'wc-mad-transbank' ),
                    'default' => 'yes'
                ),
                'title' => array(
                    'title'       => __( 'Título', 'wc-mad-transbank' ),
                    'type'        => 'text',
                    'description' => __( 'Esto es el título que ve el cliente en el checkout para esta pasarela.', 'wc-mad-transbank' ),
                    'default'     => __( 'Transbank One Click', 'wc-mad-transbank' ),
                    'desc_tip'    => true,
                ),
                'description' => array(
                    'title'       => __( 'Descripción', 'wc-mad-transbank' ),
                    'type'        => 'textarea',
                    'description' => __( 'Descripción que el cliente verá en el checkout de este método de pago.', 'wc-mad-transbank' ),
                    'default'     => __( '', 'wc-mad-transbank' ),
                    'desc_tip'    => true,
                ),
                'instructions' => array(
                    'title'       => __( 'Instrucciones', 'wc-mad-transbank' ),
                    'type'        => 'textarea',
                    'description' => __( 'Instrucciones que irán en la página de gracias y en los emails.', 'wc-mad-transbank' ),
                    'default'     => '',
                    'desc_tip'    => true,
                ),
                'ambiente' => array(
                    'title' => __('Ambiente', 'wc-mad-transbank'),
                    'type' => 'select',
                    'options' => array(
                        'INTEGRACION' => 'Integraci&oacute;n', 
                        'CERTIFICACION' => 'Certificaci&oacute;n', 
                        'PRODUCCION' => 'Producci&oacute;n'
                    ),
                    'default'     => __( 'INTEGRACION', 'wc-mad-transbank' )
                ),
                'webpay_commerce_code' => array(
                    'title' => __('C&oacute;digo de Comercio', 'wc-mad-transbank'),
                    'type' => 'text',
                    'default' => __($this->certificate['commerce_code'], 'woocommerce'),
                ),
                'webpay_private_key' => array(
                    'title' => __('Llave Privada', 'wc-mad-transbank'),
                    'type' => 'textarea',
                    'default' => __(str_replace("<br/>", "\n", $this->certificate['private_key']), 'woocommerce'),
                    'css' => 'font-family: monospace',
                ),
                'webpay_public_cert' => array(
                    'title' => __('Certificado', 'wc-mad-transbank'),
                    'type' => 'textarea',
                    'default' => __(str_replace("<br/>", "\n", $this->certificate['public_cert']), 'woocommerce'),
                    'css' => 'font-family: monospace',
                ),
                'webpay_webpay_cert' => array(
                    'title' => __('Certificado Transbank', 'wc-mad-transbank'),
                    'type' => 'textarea',
                    'default' => __(str_replace("<br/>", "\n", $this->certificate['webpay_cert']), 'woocommerce'),
                    'css' => 'font-family: monospace',
                )
                ) 
            );
        }

        /**
         * Pagina Receptora
         **/
        
        function receipt_page($order){
          echo $this->generate_transbank_payment($order);
        }

        function check_ipn_response(){
            @ob_clean();
            
            if(isset($_POST)) {
                header('HTTP/1.1 200 OK');
                $this->check_ipn_request_is_valid($_POST);
            } else {
								echo "Ocurrió un error al procesar su Compra";
            }
        }
        
				public function compose_buyorder($order){
					// TODO: It seems that the last three numbers has to be sequential from all day transactions.
					return $order->get_date_created()->date('YmdHis') . str_pad(rand(1,999), 3, '0', STR_PAD_LEFT);
        }
        
				public function check_ipn_request_is_valid($data){
					try{
            if(isset($data['TBK_TOKEN'])){
							$token_ws = $data['TBK_TOKEN'];
	          } else {
		          $token_ws = 0;
		        }

            $suscripcion = $this->wp_service->getOneClickTransaction()->finishInscription($token_ws);		
						
					} catch (Exception $e) {
						$suscripcion["error"] = "Error conectando a Webpay";
						$suscripcion["detail"] = $e->getMessage();
					}
					
					$order_session = WC()->session->get('tmp_tbk');
					          
          if((int)$suscripcion->responseCode == 0 && !empty($order_session['customer_id'])){
	          $suscripcion->tbktoken = $token_ws;
	          
						$actual = $order_session['customer_id'];
						
						error_log(print_r($suscripcion, true));
						
            update_field('field_599492332d6f7', true, 'user_'.$actual);
            
            // Autorizacion TBK
            update_field('field_599489b42d6f3', $suscripcion->authCode, 'user_'.$actual);             
            
            // Tipo Tarjeta
            update_field('field_599489e72d6f4', $suscripcion->creditCardType, 'user_'.$actual); 
            
            // Ultimos 4 digitos
            update_field('field_599491f52d6f5', $suscripcion->last4CardDigits, 'user_'.$actual); 
            
            // Usuario TBK
            update_field('field_599492092d6f6', $suscripcion->tbkUser, 'user_'.$actual); 

            $order_id = $order_session['order_id'];

            $order = wc_get_order( $order_id );
						$order->update_status('cardregistered');
            
            update_field('field_59949c6ec0e79', true, $order_id);
            update_field('field_598cb94216e21', '5-mes', $order_id);
            update_field('field_598ccc1622f3b', '30-dias', $order_id);
            
            $buyOrder = $this->compose_buyorder($order);
            
            update_post_meta( $order_id, 'buyOrder', $buyOrder);
            
            WC()->cart->empty_cart();

            wp_redirect($this->get_return_url($order));
          } else {
            wc_add_notice('Ha ocurrido un error al suscribir los datos', 'error');
            wp_redirect('/suscribete/');
          }
          die;
				}

        /**
         * Generar pago en Transbank
         **/
        function generate_transbank_payment($order_id){

            $order = new WC_Order($order_id);
            $customer = new WC_Customer($order->get_customer_id());
            $response = $this->wp_service->getOneClickTransaction()->initInscription(
                            $customer->get_username(), 
                            $customer->get_email(), 
                            $this->notify_url
                        );
            error_log(print_r($response, true));

            if($response->token){
                $sesion_tbk = array(
                    'token' => $response->token,
                    'order_id' => $order_id,
                    'customer_id' => $order->get_customer_id(),
                );
                WC()->session->set('tmp_tbk', $sesion_tbk);

                echo '<form id="FormWebpay" method="POST" action="'.$response->urlWebpay.'">';
                echo '<input type="hidden" name="TBK_TOKEN" value="'.$response->token.'">';
                echo '</form>';
                echo '<script>var form = document.getElementById("FormWebpay"); form.submit(); </script>';
            }else{
                wc_add_notice(__('ERROR: ', 'woothemes') . 'Ocurri&oacute; un error al intentar conectar con WebPay Plus. Por favor intenta mas tarde.<br/>', 'error');
            }
        }
        public function process_payment($order_id){
            $order = new WC_Order($order_id);
            return array(
                    'result' => 'success', 
                    'redirect' => $order->get_checkout_payment_url(true)
            );
        }
        
        public function thankyou_page($order_id) {
            acf_form_head();
            ?>
            <section class="woocommerce-order-details">
                <h2 class="woocommerce-order-details__title">Detalles de la Suscripción</h2>
                <p>Su suscripción está configurada con los siguientes datos:</p>
                <?php acf_form(
                        array(
                            'post_id' => $order_id,
                            'field_groups' => array('group_598cb4a6997b6'),
                            'fields' => array('field_598cb94216e21', 'field_598ccc1622f3b')
                        )
                    );
                ?>
            </section>
            <?php
            if($this->instructions){
                echo wpautop( wptexturize($this->instructions));
            }
        }

        public function email_instructions($order, $sent_to_admin, $plain_text = false){
            if($this->instructions && !$sent_to_admin && $this->id === $order->payment_method && $order->has_status('on-hold')){
                echo wpautop( wptexturize($this->instructions)).PHP_EOL;
            }
        }
        
 
        
        public function authorize_payment($order){
					try {
						$userid = $order->get_user_id();
						$customer = get_userdata($userid);
						$orderid = $order->get_order_number();
// 						$buyOrder = get_post_meta($orderid, 'buyOrder', true);
						$buyOrder = date('YmdHis') . str_pad(rand(1,999), 3, '0', STR_PAD_LEFT);
						$tbkUser = get_field('field_599492092d6f6', 'user_'.$userid); 
						$username = $customer->user_login;
						$amount = (int) number_format($order->get_total(), 0, ',', '');
						$result = $this->wp_service->getOneClickTransaction()->authorize(
							$buyOrder,
							$tbkUser,
							$username,
							$amount
						);
						$result->buyOrder = $buyOrder;
						$result->tbkUser = $tbkUser;
						$result->username = $username;
						$result->amount = $amount;
/*
					echo '<pre>';
						echo 'buyorder: ';
						print_r($buyOrder);
					echo '<br>';
					
						echo 'tbkUser: ';
						print_r($tbkUser);
					echo '<br>';
					
						echo 'username: ';
						print_r($username);
					echo '<br>';
					
						echo 'amount: ';
						print_r($amount);						
					echo '</pre>';			
*/			
					} catch (Exception $e){
						$result["error"] = "Error conectando a Webpay";
						$result["detail"] = $e->getMessage();
					}
					return $result;
        }
				public function reverse_payment($buyOrder){

	        try {
						$result = $this->wp_service->getOneClickTransaction()->reverseTransaction($buyOrder);
	        } catch (Exception $e){
		        $result["error"] = "Error conectando a Webpay";
						$result["detail"] = $e->getMessage();
	        }

					return $result;

        }
				public function unsubscribe_card($order){
					try {
						$customerid = $order->get_user_id();
						$tbkUser = get_field('field_599492092d6f6', 'user_'.$customerid); 
						$customer = get_userdata($customerid);
						$username = $customer->user_login;
						$result = $this->wp_service->getOneClickTransaction()->removeUser($tbkUser,$username);
					} catch (Exception $e){
						$result["error"] = "Error conectando a Webpay";
						$result["detail"] = $e->getMessage();	
					}
					return $result;

        }
        
        
        
    }
		//register the subscribed status
		function wc_mad_register_status() {
		    register_post_status( 'wc-cardregistered', array(
		        'label'                     => 'Tarjeta de Crédito registrada',
		        'public'                    => true,
		        'exclude_from_search'       => false,
		        'show_in_admin_all_list'    => true,
		        'show_in_admin_status_list' => true,
		        'label_count'               => _n_noop( 'Suscrito a pago <span class="count">(%s)</span>', 'Suscrito a pago <span class="count">(%s)</span>' )
		    ) );
		    register_post_status( 'wc-unregisteredcard', array(
		        'label'                     => 'Suscripción sin tarjeta registrada',
		        'public'                    => true,
		        'exclude_from_search'       => false,
		        'show_in_admin_all_list'    => true,
		        'show_in_admin_status_list' => true,
		        'label_count'               => _n_noop( 'Sin tarjeta registrada <span class="count">(%s)</span>', 'Sin tarjeta registrada <span class="count">(%s)</span>' )
		    ) );
		
		}
		
		add_action( 'init', 'wc_mad_register_status' );
		
		function wc_mad_order_statuses( $order_statuses ) {
		    $order_statuses['wc-cardregistered'] = 'Tarjeta de Crédito registrada';
				$order_statuses['wc-unregisteredcard'] = 'Sin tarjeta registrada';
		    return $order_statuses;
		}
		
		add_filter( 'wc_order_statuses', 'wc_mad_order_statuses' );
		
		function mad_oneclick_registers_submenu_page() {
		    add_submenu_page( 'woocommerce', 'One Click Registros', 'Registrados One Click', 'manage_options', 'mad_oneclick_registers', 'mad_oneclick_registers_callback' ); 
		}
		
		function mad_add_payment_action( $actions ) {
		/*
				// here are the logics for payment period
			    global $theorder;
		
		    if (get_post_meta( $theorder->id, '_wc_payment_date', true ) ) {
		        return $actions;
		    }
		*/
		    $actions['wc_charge_creditcard'] = 'Cobrar periodo a T. Crédito';
// 		    $actions['wc_reverse_payment'] = 'Reversar último pago';
				$actions['wc_unsubscribe_creditcard'] = 'Quitar registro de T. Crédito';
		    return $actions;
		}
		add_action( 'woocommerce_order_actions', 'mad_add_payment_action' );
		
		function mad_process_payment_action( $order ) {
				//add the payment logic
				$madgateway = new WC_Mad_Transbank();
				$response = $madgateway->authorize_payment($order);
				$message = '';
				
				if(is_array($response) && !empty($response['error'])){
					/*
						Array ( 
							[error] => Error conectando a Webpay (Verificar que la información del certificado sea correcta) 
							[detail] => Authorization failed 
						)
					*/
					$message = 'Error conectando a Webpay: ' . $response['detail'];
				} elseif(is_object($response)) {
					/* oneClickPayOutput Object ( 
						[authorizationCode] => 
						[creditCardType] => Visa 
						[last4CardDigits] => 6623 
						[responseCode] => -98 
						[transactionId] => 2339597 
					)
					array(
					   "0" => "Transacción aprobada",
					    "-1" => "Rechazo de transacción",
					    "-2" => "Rechazo de transacción",
					    "-3" => "Rechazo de transacción",
					    "-4" => "Rechazo de transacción",
					    "-5" => "Rechazo de transacción",
					    "-6" => "Rechazo de transacción",
					    "-7" => "Rechazo de transacción",
					    "-8" => "Rechazo de transacción",
					    "-97" => "limites Oneclick, máximo monto diario de pago excedido",
					    "-98" => "limites Oneclick, máximo monto de pago excedido",
					    "-99" => "limites Oneclick, máxima cantidad de pagos diarios excedido",
					)
					*/
						$paymentpost = array(
						  'post_title'    => 'Pago ',
						  'post_status'   => 'publish',
						  'post_type'			=> 'mad_payments'
						);
						
						$idpayment = wp_insert_post( $paymentpost );
						
						$metas = array(
								'authorizationCode' => $response->authorizationCode,
								'creditCardType' 		=> $response->creditCardType,
								'last4CardDigits' 	=> $response->last4CardDigits,
								'responseCode'			=> $response->responseCode,
								'transactionId'			=> $response->transactionId,
								'buyOrder'					=> $response->buyOrder,
								'tbkUser'						=> $response->tbkUser,
								'username'					=> $response->username,
								'amount'						=> $response->amount
						);
						
						$orderid = $order->get_order_number();
							
						add_post_meta($orderid, '_payment', $idpayment);
					
						if($response->responseCode == "0"){
							
							/*
								
								buyOrder";s:17:"20170908185420895";s:7:"
								tbkUser";s:36:"0ec2e832-1cf2-4f74-b4d8-2a4e9159241e";s:8:
								"username";s:2:"ky";s:6:
								"amount";
							*/
														
							$metas['status'] = 'Cobrado';
							
							foreach($metas as $key => $meta){
								add_post_meta($idpayment, $key, $meta, true);
							}

							$message = 'Transacción aprobada: ' . $idpayment . ' tbkUser: '.$response->tbkUser;

						} elseif($response->responseCode == "-1" || $response->responseCode == "-2" || $response->responseCode == "-3" || $response->responseCode == "-4" || $response->responseCode == "-5" || $response->responseCode == "-6" || $response->responseCode == "-7" || $response->responseCode == "-8"){
							$metas['status'] = 'Rechazo';
							foreach($metas as $key => $meta){
								add_post_meta($idpayment, $key, $meta, true);
							}
							$message = 'Error de pago: Rechazo de transacción';
						} elseif($response->responseCode == "-97"){
							$metas['status'] = 'Rechazo';
							foreach($metas as $key => $meta){
								add_post_meta($idpayment, $key, $meta, true);
							}
							$message = 'Error: Monto máximo de pago <strong>diario</strong> excedido';
						} elseif($response->responseCode == "-98"){
							$metas['status'] = 'Rechazo';
							foreach($metas as $key => $meta){
								add_post_meta($idpayment, $key, $meta, true);
							}
							$message = 'Error: Monto máximo de pago excedido';
						} elseif($response->responseCode == "-99"){
							$metas['status'] = 'Rechazo';
							foreach($metas as $key => $meta){
								add_post_meta($idpayment, $key, $meta, true);
							}
							$message = 'Error: Cantidad de pagos diarios excedida';
						} else {
							$message = 'Error desconocido 1';
							$metas['status'] = 'Error';
							foreach($metas as $key => $meta){
								add_post_meta($idpayment, $key, $meta, true);
							}
							error_log(print_r($response, true));
						}
				} else {
					$message = 'Error desconocido 2';
					error_log(print_r($response, true));
				}

				//process
				
				//output if success or failure
		    
// 		    $message = sprintf( 'Cobro a Tarjeta de Crédito a usuario %s .', wp_get_current_user()->display_name ); //TODO: poner fecha/periodo en vez de display_name
		    $order->add_order_note( $message );
		    
		    // add a flag, can be used in mad_add_payment_action()
		//     update_post_meta( $order->id, '_wc_order_marked_printed_for_packaging', 'yes' );
		}
		
		add_action( 'woocommerce_order_action_wc_charge_creditcard', 'mad_process_payment_action' );
		
		function mad_process_reverse_action( $order ) {
				//add the payment logic
				$madgateway = new WC_Mad_Transbank();
				$ble = $madgateway->reverse_payment($order);
				print_r($ble);
				exit;
				
				//process
				
				//output if success or failure
		    
		    $message = sprintf( 'Reversa de pago a usuario %s .', wp_get_current_user()->display_name ); //TODO: poner fecha/periodo en vez de display_name
		    $order->add_order_note( $message );
		    
		    // add a flag, can be used in mad_add_payment_action()
		//     update_post_meta( $order->id, '_wc_order_marked_printed_for_packaging', 'yes' );
		}
		
		add_action( 'woocommerce_order_action_wc_reverse_payment', 'mad_process_reverse_action' );
		
		function mad_process_unsubscribe_action( $order ) {
				//add the unsusbscribe logic
				$madgateway = new WC_Mad_Transbank();
				$response = $madgateway->unsubscribe_card($order);
				$message = '';
				if(!empty($response) && $response === TRUE){
			    $message = 'Quitado registro de pago automático para esta suscripción.';
					$order->update_status('unregisteredcard');
				} else {
					$message = 'Error desuscribiendo Tarjeta de Crédito.';
					error_log(print_r($response, true));
				}
		    $order->add_order_note($message);		    
		}
		
		add_action( 'woocommerce_order_action_wc_unsubscribe_creditcard', 'mad_process_unsubscribe_action' );
		
		
		

}